using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using StomaWebApp.Controllers.Data;
using System;
using System.Text;

namespace StomaWebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            IocContainer.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //Add application DbContext to DI
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(IocContainer.Configuration.GetConnectionString("DefaultConnection")));
            services.AddControllersWithViews();

            //AddIdentity adds cookie based authentication
            //Adds scoped classes for things like UserManager, SignInManager
            //NOTE: Automatically adds the validated user from a cookie to the HttpContext.user
            services.AddIdentity<ApplicationUser, IdentityRole>()
                //Adds UserStore and RoleStore from this context
                .AddEntityFrameworkStores<ApplicationDbContext>()
                //Adds a provider that generates unique keys and hashes for things like
                //forgot password links phone number, verification codes etc...
                .AddDefaultTokenProviders();

            //Add JWT configuration
            services.AddAuthentication().AddJwtBearer(options => {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = IocContainer.Configuration["Jwt:Issuer"],
                    ValidAudience = IocContainer.Configuration["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(IocContainer.Configuration["Jwt:SecretKey"])),
                };
            });

            //Change login URL
            //Change cookie timeout

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 5;
                options.Password.RequireLowercase = true;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
            });

            services.ConfigureApplicationCookie(options =>
            {
                //redirect to /login
                options.LoginPath = "/Login/LoginPage";
                //Cookie timeout to expire in 3600 seconds
                options.ExpireTimeSpan = TimeSpan.FromSeconds(3600);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            //Store instance of the DI Service provider
            IocContainer.Provider = serviceProvider as ServiceProvider;

            //setup identity
            app.UseAuthentication();

            // var userStore = IocContainer.Provider.GetService<UserManager<ApplicationUser>>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
