﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StomaWebApp.Views.ViewModels
{
    public class AppointmentViewModel
    {
        [Required]
        public string Date { get; set; }
        [Required]
        public string Time { get; set; }
        [Required]
        public string PatientName { get; set; }
    }
}
