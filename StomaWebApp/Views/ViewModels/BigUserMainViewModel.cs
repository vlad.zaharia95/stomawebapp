﻿using StomaWebApp.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StomaWebApp.Views.ViewModels
{
    public class BigUserMainViewModel
    {
        [Required]
        public AppointmentViewModel AppViewModel { get; set; }
        [Required]
        public List<string> patientNames { get; set; }
        [Required]
        public List<AppointmentDataModel> AllAppointments { get; set; }
        [Required]
        public List<PatientDataModel> AllPatients { get; set; }
        [Required]
        public int CurrentPage { get; set; }
        public string NewPatient { get; set; }
        public DateTime NewDate { get; set; } = DateTime.Today;
        public string DateTimeString { get; set; }
    }


}
