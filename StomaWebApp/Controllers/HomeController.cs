﻿
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StomaWebApp.Controllers.Data;
using StomaWebApp.Models;

namespace StomaWebApp.Controllers
{
    #region Protected members
    #endregion
    /// <summary>
    /// The scoped application context
    /// </summary>
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        protected ApplicationDbContext mContext;
        public HomeController(
            ILogger<HomeController> logger,
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _logger = logger;
            mContext = context;

            mUserManager = userManager;
            mSignInManager = signInManager;
        }

        /// <summary>
        /// The manager for handling users CRUD, searching, roles
        /// </summary>
        protected UserManager<ApplicationUser> mUserManager;
        /// <summary>
        /// the manager for handling signing in and out our users
        /// </summary>
        protected SignInManager<ApplicationUser> mSignInManager;

        public IActionResult Index()
        {
            mContext.Database.EnsureCreated();

            //if (!mContext.Settings.Any())
            //{
            //    mContext.Settings.Add(new Data.SettingsDataModel
            //    {
            //        Id = "1",
            //        Name = "BackgroundColor",
            //        Value = "Red"
            //    });

            //    var settingsLocally = mContext.Settings.Local.Count();
            //    var settingsDatabase = mContext.Settings.Count();

            //    var firstLocal = mContext.Settings.Local.FirstOrDefault();
            //    var firstDatabase = mContext.Settings.FirstOrDefault();

            //    mContext.SaveChanges();

            //    settingsLocally = mContext.Settings.Local.Count();
            //    settingsDatabase = mContext.Settings.Count();

            //    firstLocal = mContext.Settings.Local.FirstOrDefault();
            //    firstDatabase = mContext.Settings.FirstOrDefault();
            //}

            return View();
        }
        [Route("create")]
        public async Task<IActionResult> CreateUserAsync()
        {
            var result = await mUserManager.CreateAsync(new ApplicationUser
            {
                UserName = "vlad",
                Email = "vlad@vlad.com"
            }, "password");
            if (result.Succeeded)
                return Content("User was created", "text/html");

            return Content("User creation failed", "text/html");
        }
        //private area
        [Authorize]
        [Route("private")]
        public IActionResult Private()
        {
            var isLoggedin = HttpContext.User.Identity.IsAuthenticated;
            return Content($"This is a private area. Welcome {HttpContext.User.Identity.Name}", "text/html");
        }
        /// <summary>
        /// An auto login page for testing
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [Route("login")]
        public async Task<IActionResult> LoginAsync(string returnUrl)
        {

            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);

            var result = await mSignInManager.PasswordSignInAsync("vlad", "password", true, false);
            if (result.Succeeded)
            {
                //if we have no return url
                if (string.IsNullOrEmpty(returnUrl))
                    return RedirectToAction(nameof(Index));//go home

                return Redirect(returnUrl);//go to return url

            }
            return Content("Failed to log in", "text/html");
        }

       
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
             return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
