﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StomaWebApp.Controllers.Data;
using StomaWebApp.Data;
using StomaWebApp.Views.ViewModels;
using System.Web;
using System.Globalization;

namespace StomaWebApp.Controllers.LoggedInController
{
    public class UserMainController : Controller
    {
        protected ApplicationDbContext mContext;
        public static List<string> patientNames;
        private BigUserMainViewModel bigViewModel;

        public UserMainController(ApplicationDbContext context)
        {
            mContext = context;
            bigViewModel = new BigUserMainViewModel();
            bigViewModel.CurrentPage = 1;
        }

        [Authorize]
        public IActionResult UserMain(BigUserMainViewModel model)
        {

            if (ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Something went wrong");
            }

            bigViewModel.AllAppointments = GetAppointmentsWithDate(model);

            bigViewModel.patientNames = GetPatientNames();

            bigViewModel.NewDate = model.NewDate;

            return View(bigViewModel);
        }
        [Authorize]
        public IActionResult Patients(int id)
        {
            bigViewModel.CurrentPage = id;
            var total = mContext.Patient.ToList().Count;
            var pageSize = 10; // set your page size, which is number of records per page

            var page = id; // set current page number, must be >= 1 (ideally this value will be passed to this logic/function from outside)

            var skip = pageSize * (page - 1);

            var tooBig = (skip < total);
            var tooSmall = (id < 1);

            if (!tooBig || tooSmall)// do what you wish if you can page no further
            {
                if (!tooBig)
                {
                    id -= 1;
                    return RedirectToAction(nameof(Patients), new { id=id});
                }
                else
                {
                    id += 1;
                    return RedirectToAction(nameof(Patients), new { id = id });

                }
            }
                var patientsPerPage = mContext.Patient
                         .Skip(skip)
                         .Take(pageSize)
                         .ToList();

            bigViewModel.AllPatients = patientsPerPage;
            return View(nameof(Patients), bigViewModel);
        }

        public IActionResult AddAppointment(BigUserMainViewModel model)
        {
            var patient = (from name in mContext.Patient
                           where name.Name == model.AppViewModel.PatientName
                           select name).FirstOrDefault();
            
            if (patient == null) {
                
                return RedirectToAction(nameof(UserMain));
            }
            string stringDateTime = model.AppViewModel.Date + " " + model.AppViewModel.Time;
            DateTime dateTime = DateTime.ParseExact(stringDateTime, "dd/MM/yyyy HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture);
            AppointmentDataModel data = new AppointmentDataModel
            {
                Id = Guid.NewGuid().ToString(),
                DateTime = dateTime,
                PatientName = patient.Name
            };
            mContext.Add<AppointmentDataModel>(data);
            mContext.SaveChanges();
            return RedirectToAction(nameof(UserMain));
        }

        public IActionResult UpdateAppointment(BigUserMainViewModel model, string id)
        {
            if(model.AppViewModel.Time == null || model.AppViewModel.Date == null)
            {
                return RedirectToAction(nameof(UserMain));
            }
            var patient = (from name in mContext.Patient
                           where name.Name == model.AppViewModel.PatientName
                           select name).FirstOrDefault();

            var toUpdate = (from appointment in mContext.Appointment
                            where appointment.Id == id
                            select appointment).FirstOrDefault();

            if (patient == null || toUpdate == null)
            {
                return RedirectToAction(nameof(UserMain));
            }

            string stringDateTime = model.AppViewModel.Date + " " + model.AppViewModel.Time;
            DateTime dateTime = DateTime.ParseExact(stringDateTime, "dd/MM/yyyy HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture);
            toUpdate.DateTime = dateTime;
            mContext.Update<AppointmentDataModel>(toUpdate);
            mContext.SaveChanges();
            return RedirectToAction(nameof(UserMain));
        }

        public IActionResult DeleteAppointment(string id)
        {
            var toDelete = (from appointment in mContext.Appointment
                               where appointment.Id == id
                               select appointment).FirstOrDefault();

            mContext.Remove<AppointmentDataModel>(toDelete);
            mContext.SaveChanges();
            return RedirectToAction(nameof(UserMain));
        }

        public IActionResult AddPatient(BigUserMainViewModel model)
        {
            var patient = (from name in mContext.Patient
                           where name.Name == model.NewPatient
                           select name).FirstOrDefault();

            if (patient != null)
            {
                return RedirectToAction(nameof(Patients));
            }

            PatientDataModel data = new PatientDataModel
            {
                Id = Guid.NewGuid().ToString(),
                Name = model.NewPatient,
            };
            mContext.Add<PatientDataModel>(data);
            mContext.SaveChanges();
            return RedirectToAction(nameof(Patients));
        }

        public List<string> GetPatientNames()
        {
            var patients = mContext.Patient;
            var patientNames = patients.Select(name => name.Name).ToList();
            return patientNames;
        }

        public List<AppointmentDataModel> GetAppointmentsWithDate(BigUserMainViewModel model)
        {
            var appointments = mContext.Appointment.ToList();
            appointments = appointments.OrderBy(a => a.DateTime).Where(a => a.DateTime >= model.NewDate).ToList();
            return appointments;
        }
        public List<PatientDataModel> GetAllPatients()
        {
            var patients = mContext.Patient.ToList();
            return patients;
        }

        public IActionResult ChangeDate(BigUserMainViewModel model)
        {
            if (model.DateTimeString != null)
            {
                bigViewModel.NewDate = DateTime.ParseExact(model.DateTimeString, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var appointments = mContext.Appointment.ToList();
                appointments = appointments.OrderBy(a => a.DateTime).Where(a => a.DateTime >= bigViewModel.NewDate).ToList();
                return RedirectToAction(nameof(UserMain), bigViewModel);
            }
            return RedirectToAction(nameof(UserMain), bigViewModel);
        }
    }
}