﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StomaWebApp;
using StomaWebApp.Controllers.Data;
using StomaWebApp.Views.ViewModels;

namespace StomaProjectWeb.Controllers.LoginControllers
{
    public class LoginController : Controller
    {

        protected ApplicationDbContext mContext;
        public LoginController(
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            mContext = context;

            mUserManager = userManager;
            mSignInManager = signInManager;
        }

        /// <summary>
        /// The manager for handling users CRUD, searching, roles
        /// </summary>
        protected UserManager<ApplicationUser> mUserManager;
        /// <summary>
        /// the manager for handling signing in and out our users
        /// </summary>
        protected SignInManager<ApplicationUser> mSignInManager;

        [HttpGet]
        public IActionResult LoginPage()
        {
           return View();
        }

        [HttpPost]
        public async Task<IActionResult> LoginPage(LoginUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);

                var result = await mSignInManager.PasswordSignInAsync(model.Email, model.Password, true, false);
                if (result.Succeeded)
                {
                    return RedirectToAction("UserMain", "UserMain");
                }
                else
                {
                    ModelState.AddModelError("Invalid credentials", "Invalid Credentials");
                    return View(model);
                }

            }
            return View(model);
        }

        public IActionResult RegisterPage()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> RegisterPage(RegisterUserViewModel model)
        {
            if (ModelState.IsValid)
            {
               
                var result = await mUserManager.CreateAsync(new ApplicationUser
                {
                    UserName = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName
                }, model.Password) ;

                if (result.Succeeded)
                    return RedirectToAction(nameof(LoginPage));

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(model);
            
        }

        [Route("logout")]
        public async Task<IActionResult> SingOutTask()
        {
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            return RedirectToAction(nameof(LoginPage));
        }
    }
}