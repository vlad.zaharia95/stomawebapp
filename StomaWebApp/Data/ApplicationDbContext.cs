﻿using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using StomaWebApp.Data;

namespace StomaWebApp.Controllers.Data
{
    /// <summary>
    ///
    /// </summary>
    /// 
    
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        #region Public Properties 
        public DbSet<SettingsDataModel> Settings { get; set; }
        public DbSet<PatientDataModel> Patient { get; set; }
        public DbSet<AppointmentDataModel> Appointment { get; set; }
        public DbSet<SurgeryDataModel> Surgery { get; set; }


        #endregion


        /// <summary>
        /// Default constructor expecting database options passed in
        /// </summary>
        /// <param name="options"></param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<SettingsDataModel>().HasIndex(a => a.Name);
            modelBuilder.Entity<PatientDataModel>().HasIndex(a => a.Name).IsUnique();
        }
    }
}
