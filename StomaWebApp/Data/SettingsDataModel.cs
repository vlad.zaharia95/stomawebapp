﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StomaWebApp.Controllers.Data
{
    /// <summary>
    /// Settings database table representational model
    /// </summary>
    public class SettingsDataModel
    {
        /// <summary>
        /// Unique Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Settings Name
        /// </summary>
        ///<remarks>This is indexed</remarks>
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }
        /// <summary>
        /// Settings Value
        /// </summary>
        [MaxLength(2048)]
        public string Value { get; set; }
    }
}
