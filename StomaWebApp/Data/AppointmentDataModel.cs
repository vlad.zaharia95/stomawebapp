﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StomaWebApp.Data
{
    public class AppointmentDataModel
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public DateTime DateTime { get; set; }
        [Required]
        public string PatientName { get; set; }
        public SurgeryDataModel Surgery { get; set; }
    }
}
