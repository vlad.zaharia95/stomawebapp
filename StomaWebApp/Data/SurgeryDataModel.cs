﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StomaWebApp.Data
{
    public class SurgeryDataModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public PatientDataModel Patient { get; set; }
    }
}
