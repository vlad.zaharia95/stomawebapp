﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StomaWebApp.Migrations
{
    public partial class addedsurgerytoappointment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Appointment_Patient_patientId",
                table: "Appointment");

            migrationBuilder.RenameColumn(
                name: "patientId",
                table: "Appointment",
                newName: "PatientId");

            migrationBuilder.RenameIndex(
                name: "IX_Appointment_patientId",
                table: "Appointment",
                newName: "IX_Appointment_PatientId");

            migrationBuilder.AddColumn<string>(
                name: "SurgeryId",
                table: "Appointment",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Appointment_SurgeryId",
                table: "Appointment",
                column: "SurgeryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Appointment_Patient_PatientId",
                table: "Appointment",
                column: "PatientId",
                principalTable: "Patient",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Appointment_Surgery_SurgeryId",
                table: "Appointment",
                column: "SurgeryId",
                principalTable: "Surgery",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Appointment_Patient_PatientId",
                table: "Appointment");

            migrationBuilder.DropForeignKey(
                name: "FK_Appointment_Surgery_SurgeryId",
                table: "Appointment");

            migrationBuilder.DropIndex(
                name: "IX_Appointment_SurgeryId",
                table: "Appointment");

            migrationBuilder.DropColumn(
                name: "SurgeryId",
                table: "Appointment");

            migrationBuilder.RenameColumn(
                name: "PatientId",
                table: "Appointment",
                newName: "patientId");

            migrationBuilder.RenameIndex(
                name: "IX_Appointment_PatientId",
                table: "Appointment",
                newName: "IX_Appointment_patientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Appointment_Patient_patientId",
                table: "Appointment",
                column: "patientId",
                principalTable: "Patient",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
