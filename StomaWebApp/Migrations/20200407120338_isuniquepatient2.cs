﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StomaWebApp.Migrations
{
    public partial class isuniquepatient2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Patient_Name",
                table: "Patient");

            migrationBuilder.CreateIndex(
                name: "IX_Patient_Name",
                table: "Patient",
                column: "Name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Patient_Name",
                table: "Patient");

            migrationBuilder.CreateIndex(
                name: "IX_Patient_Name",
                table: "Patient",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");
        }
    }
}
