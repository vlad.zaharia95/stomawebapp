﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StomaWebApp.Controllers.Data;

namespace StomaWebApp
{
    public static class IoC
    {
        public static ApplicationDbContext ApplicationDbContext => IocContainer.Provider.GetService<ApplicationDbContext>();
    }
    public static class IocContainer
    {
        public static ServiceProvider Provider { get; set; }

        public static IConfiguration Configuration { get; set; }
    }
}
